from .views import list_projects, project_detail_view, create_project_view
from django.urls import path

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", project_detail_view, name="show_project"),
    path("create/", create_project_view, name="create_project"),
]
