from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import LoginForm, SignUpForm

# Create your views here.


def signup_view(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            password = form.cleaned_data.get("password")
            password_confirmation = form.cleaned_data.get(
                "password_confirmation"
            )
            if password == password_confirmation:
                user = form.save(commit=False)
                user.set_password(password)
                user.save()
                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error(
                    "password_confirmation", "The passwords do not match."
                )
    else:
        form = SignUpForm()
    context = {"form": form}
    return render(request, "registration/signup.html", context)


def logout_view(request):
    logout(request)
    return redirect("login")


def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("list_projects")
    else:
        form = LoginForm()
    context = {"form": form}
    return render(request, "login.html", context)
