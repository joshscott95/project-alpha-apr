from django.shortcuts import render, redirect
from .forms import TaskForm
from .models import Task
from django.contrib.auth.decorators import login_required


@login_required
def show_tasks_view(request):
    user = request.user
    tasks = Task.objects.filter(assignee=user)
    context = {"tasks": tasks}
    return render(request, "list_tasks.html", context)


@login_required
def create_task_view(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "create_task.html", context)
